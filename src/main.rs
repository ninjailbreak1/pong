use ggez;
use ggez::event;
use ggez::graphics;
use ggez::nalgebra as na;
use ggez::{Context, GameResult};
use ggez::event::{KeyCode, KeyMods};

use rand::Rng;

const SCREEN_MAX_X: f32 = 800.0;
const SCREEN_MAX_Y: f32 = 600.0;

#[derive(Copy, Clone)]
enum PlayerType {
    Blue,
    Red
}

struct Player {
    ptype: PlayerType,
    pos_x: f32,
    pos_y: f32,
    score: u32,
}

impl Player {
    fn new(PType: PlayerType) -> Player {
        match PType{
            Blue => Player { ptype: PType, pos_x: 100.0, pos_y: 300.0, score: 0 },
            Red => Player { ptype: PType, pos_x: 700.0, pos_y: 300.0, score: 0 },
        }
    }
}

struct Ball {
    pos_x: f32,
    pos_y: f32,
    angle: f32,
    radius: f32,
    velocity: f32
}

impl Ball {
    fn new() -> Ball {
        let mut rng = rand::thread_rng();
        Ball { 
            pos_x: 400.0,
            pos_y: 300.0,
            angle: rng.gen_range(0.0, 2.0 * 3.14159265358979323846264338327950288_f32), 
            radius: 20.0,
            velocity: 6.0
        }
    }

    fn collision(&mut self) {
        if self.pos_x - self.radius < 0.0 {
            self.pos_x = self.pos_x.abs();
            self.angle = 3.14159265358979323846264338327950288_f32 - self.angle;
        } else if self.pos_x + self.radius > SCREEN_MAX_X {
            self.pos_x = SCREEN_MAX_X * 2.0 - self.pos_x - 2.0 * self.radius;
            self.angle = 3.14159265358979323846264338327950288_f32 - self.angle;
        }
    }
}

struct MainState {
    Player1: Player,
    Player2: Player,
    Blaster: Ball
}

impl MainState {
    fn new() -> GameResult<MainState> {
        let s = MainState { 
            Player1: Player::new(PlayerType::Blue),
            Player2: Player::new(PlayerType::Red),
            Blaster: Ball::new() 
        };

        Ok(s)
    }
}

impl event::EventHandler for MainState {
    fn update(&mut self, _ctx: &mut Context) -> GameResult {
        self.Blaster.pos_x += self.Blaster.velocity * self.Blaster.angle.cos();
        self.Blaster.pos_y += self.Blaster.velocity * self.Blaster.angle.sin();
        self.Blaster.collision();
        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult {
        graphics::clear(ctx, [0.1, 0.2, 0.3, 1.0].into());

        let circle = graphics::Mesh::new_circle(
            ctx,
            graphics::DrawMode::fill(),
            na::Point2::new(0.0, 0.0),
            self.Blaster.radius,
            1.0,
            graphics::WHITE,
        )?;
        graphics::draw(ctx, &circle, (na::Point2::new(self.Blaster.pos_x, self.Blaster.pos_y),))?;

        graphics::present(ctx)?;
        Ok(())
    }

    fn key_down_event(&mut self, ctx: &mut Context, keycode: KeyCode, _keymods: KeyMods, _repeat: bool) {
        let mut rng = rand::thread_rng();
        self.Blaster.angle = match keycode {
            KeyCode::Space => rng.gen_range(0.0, 2.0 * 3.14159265358979323846264338327950288_f32),
            _ => self.Blaster.angle
        };
    }
}

pub fn main() -> GameResult {
    let cb = ggez::ContextBuilder::new("super_simple", "ggez");
    let (ctx, event_loop) = &mut cb.build()?;
    let state = &mut MainState::new()?;
    event::run(ctx, event_loop, state)
}
